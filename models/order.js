const monggose = require('mongoose');

const Schema = monggose.Schema;

const orderSchema = new Schema({
    products: [{
            product: {type :Object , required: true},
            quantity: { type: Number, required: true },

      }],
    user : {
        userId : {
            type : monggose.Types.ObjectId,
            ref : 'User',
            required : true
        }, 
        email : {
            type : String,
            required : true
        }
    }

})

module.exports = monggose.model('Order',orderSchema)