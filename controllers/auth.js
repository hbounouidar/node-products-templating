const bcrypt = require('bcryptjs');
const crypto = require('crypto');

const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const  {validationResult} = require('express-validator/check')

const User = require('../models/user');

const transporter = nodemailer.createTransport(sendgridTransport({
    auth : {
        api_key : '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',

    }
}))

exports.getLogin = (req, res, next) => {
    let message = req.flash('error');
    message = (message.length > 0) ? message[0] : null;

  res.render('auth/login', {
    path: '/login',
    pageTitle: 'Login',
    errorMessage : message,
    oldInput: {
        email: "",
        password: ""
      },
      validationErrors: []

  });
};

exports.getSignup = (req, res, next) => {
    let message = req.flash('error');
    message = (message.length > 0) ? message[0] : null;
  res.render('auth/signup', {
    path: '/signup',
    pageTitle: 'Signup',
    errorMessage : message,
    oldInput: {
        email: "",
        password: "",
        confirmPassword : ""
      },
      validationErrors: []
  });
};

exports.postLogin = (req, res, next) => {
 const email = req.body.email;
 const pwd = req.body.password;

 const errors = validationResult(req);
 if (!errors.isEmpty()){
    return res.status(422).render('auth/login', {
        path: '/login',
        pageTitle: 'Login',
        errorMessage : errors.array()[0].msg,
        oldInput: {
            email: email,
            password: pwd
            },
          validationErrors: errors.array()
    
      });
 }
  User.findOne({email : email})
    .then(user => {
        if(!user){
            return res.status(422).render('auth/login', {
                path: '/login',
                pageTitle: 'Login',
                errorMessage : errors.array()[0].msg,
                oldInput: {
                    email: email,
                    password: pwd
                    },
                  validationErrors: []
            
              });
        }
        bcrypt.compare(pwd, user.password)
        .then(doMatch => {
            if (doMatch){
                
                req.session.isLoggedIn = true;
                req.session.user = user;
                return req.session.save(err => {
                  console.log(err);
                   res.redirect('/')
                });
            }
            return res.status(422).render('auth/login', {
                path: '/login',
                pageTitle: 'Login',
                errorMessage : errors.array()[0].msg,
                oldInput: {
                    email: email,
                    password: pwd
                    },
                  validationErrors: []
            
              });
        })
        .catch(err => {
            console.log(err);
            res.redirect('/login')
        })

    })
    .catch((err) => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        next(error);
      });};

exports.postSignup = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    const errors = validationResult(req);
    if (!errors.isEmpty()){
        return res.status(422).render("auth/signup", {
          path: "/signup",
          pageTitle: "Signup",
          errorMessage: errors.array()[0].msg,
          oldInput: {
            email: email,
            password: password,
            confirmPassword : req.body.confirmPassword,
            },
          validationErrors: errors.array()
        });
    }

    bcrypt
        .hash(password, 12)
        .then(hashedPwd => {
            const user = new User({
                email: email,
                password:hashedPwd,
                cart : { items : []}
            })
            return user.save();

        })
        .then(result => {
            res.redirect('/login')
            transporter.sendMail({
                to :email,
                from : 'bounouidarhamza@outlook.fr',
                subject : 'Signup successed ! ',
                html : '<h1>You are successfuly signed up !</h1>'
            })
            .catch(err => console.log(err))
        })
};

exports.postLogout = (req, res, next) => {
  req.session.destroy(err => {
    console.log(err);
    res.redirect('/');
  });
};

exports.getReset = (req,res,next) => {
    let message = req.flash('error');
    message = (message.length > 0) ? message[0] : null;
    res.render('auth/reset', {
        path: '/reset',
        pageTitle: 'Reset',
        errorMessage : message
    
      });
}

exports.postReset = (req,res,next) => {
    crypto.randomBytes(32, (err, buffer) => {
        if (err){
            console.log(err);
            return res.redirect('/reset')
        }
        const token = buffer.toString('hex');
        User.findOne({email : req.body.email})
            .then(user => {
                if (!user){
                    req.flash('error', 'No account with the email found');
                    return res.redirect('/reset');

                }
                user.resetToken = token;
                user.resetTokenExpiration = Date.now() + 3600000;
                return user.save()
            })
            .then(result => {
                res.redirect('/');
                transporter.sendMail({
                    to :req.body.email,
                    from : 'bounouidarhamza@outlook.fr',
                    subject : 'Password reset',
                    html : `
                        <p>You requested a password request</p>
                        <p>Click this 
                            <a href="http://localhost:3000/reset/${token}"> link</a>
                            to set a new password
                        </p>
                    `
                })
            })
            .catch((err) => {
                const error = new Error(err);
                error.httpStatusCode = 500;
                next(error);
              });    })
}

exports.getNexPassword = (req,res,next) => {

    const token = req.params.token ;
    User.findOne({resetToken : token, resetTokenExpiration : {$gt : Date.now()}})
        .then(user => {
            let message = req.flash('error');
            message = (message.length > 0) ? message[0] : null;
            res.render('auth/new-password', {
                path: '/new-password',
                pageTitle: 'New Password',
                errorMessage : message,
                passwordToken : token,
                userId : user._id.toString()
            
              });
        })
        .catch((err) => {
            const error = new Error(err);
            error.httpStatusCode = 500;
            next(error);
          });
}

exports.postNewPassword = (req,res,next) => {
    const newPassword = req.body.password;
    const userId = req.body.userId;
    const passwordToken = req.body.passwordToken;
    let resetUser; 
    User.findOne({
      resetToken: passwordToken,
      resetTokenExpiration: { $gt: Date.now() },
      _id : userId
    })
      .then((user) => {
        if (!user){
            req.flash('error', 'Unfound user or token expired')
        }
        resetUser = user;
        return bcrypt.hash(newPassword , 12);
      })
      .then(hashedPwd => {
          resetUser.password = hashedPwd;
          resetUser.resetToken = undefined;
          resetUser.resetTokenExpiration = undefined;
          resetUser.save();
      })
      .then((result) => {
        res.redirect("/login");
      })
      .catch((err) => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        next(error);
      });}
