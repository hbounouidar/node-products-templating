const path = require('path');

const express = require('express');

const adminController = require('../controllers/admin');
const {body} = require('express-validator/check');

const isAuth = require('../middleware/is-auth');

const router = express.Router();

//GET
router.get(
    '/add-product',isAuth, adminController.getAddProduct); 

// //GET
 router.get('/products',isAuth, adminController.getProducts);
 

// //POST
router.post('/add-product',
[
    body('title').isString().isLength({min : 2}),
    //body('imageUrl').isURL(),
    body('price').isFloat(),
    body('description').isLength({min : 2}),

],
isAuth, adminController.postAddProduct );

router.get('/edit-product/:productId',isAuth,  adminController.getEditProduct);

router.post('/edit-product',
[
    body('title').isString().isLength({min : 2}),
   // body('imageUrl').isURL(),
    body('price').isFloat(),
    body('description').isLength({min : 2}),

],
isAuth, adminController.postEditProduct);

router.delete('/product/:productId',isAuth, adminController.deleteProduct);
 
exports.routes = router;