const express = require('express');
const {check, body} = require('express-validator/check');

const User = require('../models/user');
const authController = require('../controllers/auth');

const router = express.Router();

router.get('/login', authController.getLogin);

router.get('/signup', authController.getSignup);

router.post(
    '/login', 
    [
        body('email')
        .isEmail()
        .withMessage('Please enter a valid email !')
        .normalizeEmail()
         ,
        check('password', 'Password must be valid min 2')
        .isLength({min : 2}) 
        .trim()
    ],
    authController.postLogin);

router.post(
  "/signup",
    [  
   check("email")
        .isEmail()
        .withMessage("Please enter valid email")
        .custom((value,{req}) => {
            // if (value === 'toto@a.com'){
            //     throw new Error('This email adress is not authorized')
            // }
            // return true;
                
        return  User
                .findOne({email : value})
                .then(userDoc => {
                    if (userDoc){
                        return Promise.reject(
                            'Email already exists')
                    }
                })

        })
        .normalizeEmail(),
  body( 'password',
            'Please enter a Password with only members and text and at least 5 characters'
            )
        .isLength({min : 5})
        .isAlphanumeric()
        .trim(),
  body('confirmPassword')
            .custom((value, {req}) => {
            if (value !== req.body.password){
                throw new Error('Passwords must match')
            }
            return true;
        }).trim()
],
  authController.postSignup
);

router.post('/logout', authController.postLogout);

router.get('/reset', authController.getReset);

router.post('/reset', authController.postReset);

router.get('/reset/:token', authController.getNexPassword);

router.post('/new-password', authController.postNewPassword);

module.exports = router;