
const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const https = require('https');

const mongoose = require('mongoose');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');
const multer = require('multer');
const helmet = require('helmet');
const compression = require('compression');
const morgan = require('morgan');

const path = require('path');
//const expressHbs = require('express-handlebars');
const errorController = require('./controllers/error');

const User = require('./models/user');

const MANGODB_URI = 
`mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0-2nbnv.mongodb.net/${process.env.MONGO_DATABASE}`;



const app = express();
const store = new MongoDBStore({
    uri : MANGODB_URI,
    collection : 'sessions'
});


//done with horoku
// const privateKey = fs.readFileSync('server.key');
// const certificate = fs.readFileSync('server.cert');

const csrfProtection = csrf();

const fileStorage = multer.diskStorage({
    destination : (req,file, cb) => {
        cb(null, 'images')
    },
    filename : (req,file,cb) => {
        // const name = new Date().toDateString()+ '-' +file.originalname;
        const name = (new Date().toISOString()+ '-' +file.originalname).split(':').join('-').split('-').join('');
        cb(null, name)
    }
});
const fileFilter = (req,file,cb) => {
    if (file.mimetype ==='image/png' 
        || file.mimetype ==='image/jpg'
        || file.mimetype ==='image/jpeg') {
            cb(null,true);

        }else {
            cb(null,false)

        }
}

//EJS
app.set("view engine", "ejs"); 
app.set('views','views');


const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop'); 
const authRoutes = require('./routes/auth'); 

const accessLogStream = fs.createWriteStream(
    path.join(__dirname, 'access.log'),
    {flags : 'a'} //append
)

app.use(helmet());
app.use(compression());
app.use(morgan('combined', {stream : accessLogStream}))

app.use(bodyParser.urlencoded({extended:false}));
app.use(
  multer({
    dest: "images",
    storage: fileStorage,
    fileFilter : fileFilter
  }).single("image")
);

app.use(express.static(path.join(__dirname, 'public')));
app.use('/images',express.static(path.join(__dirname, 'images')));
app.use(
  session({
    secret: "my secret",
    resave: false,
    saveUninitialized : false,
    store:store
  })
);

app.use(csrfProtection);
app.use(flash())

app.use((req,res,next)=> {
    res.locals.isAuthenticated = req.session.isLoggedIn;          
    res.locals.csrfToken = req.csrfToken(); 
    next();       
})


app.use((req,res,next) => {
      if (!req.session.user){
          return next();
      }
     User.findById(req.session.user._id)
    .then(user => {
       
        if(!user) {
            next();
        }
        req.user = user;
        next();
    })
    .catch(err =>{
        next(new Error(err));
        }); 

})  


app.use('/admin',adminRoutes.routes);
app.use(shopRoutes); 
app.use(authRoutes); 

app.get('/500', errorController.get500);

app.use(errorController.get404);

app.use((error, req,res,next) => {
  //  res.redirect('/500')
  console.log(error.message);
    res.status(500).render("500", {
        pageTitle: "Server Error !",
        path: "/500",
        isAuthenticated:req.session.isLoggedIn
      });
});


mongoose
    .connect(MANGODB_URI)
    .then(result => {
        app.listen(process.env.PORT || 3000);
        // https.createServer({
        //     key : privateKey,
        //     cert : certificate
        // },
        // app)
        // .listen(process.env.PORT || 3000)
    })
    .catch(err => {
        console.log(err)
    })
